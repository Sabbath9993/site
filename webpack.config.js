const path = require("path");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const precss = require('precss');
const postcssImport = require('postcss-import');
const postcssRgb = require('postcss-rgb');
const postcssFlexbugsFixes = require('postcss-flexbugs-fixes');
const postcssNestedAncestors = require('postcss-nested-ancestors');
const postcssOmitImportTilde = require('postcss-omit-import-tilde');
const postcssCalc = require('postcss-calc');
const autoprefixer = require('autoprefixer');
const lost = require('lost');

let mode = process.env.NODE_ENV === "production" ? "production": "development";
let target = process.env.NODE_ENV === "production" ? "browserslist": "web";


const isHotModuleReplacementEnabled = mode === "development";

const cssLoaderModulesOptions = {
    mode: 'local',
    localIdentName: mode === "development" ? '[folder]__[local]_[hash:base64:6]' : '[hash:base64:6]'
};

const postCssLoader = {
    loader: 'postcss-loader',
    options: {
        postcssOptions: {
            parser: 'postcss-scss',
            plugins: [
                postcssOmitImportTilde,
                postcssImport({
                    path: [__dirname],
                    plugins: [postcssOmitImportTilde]
                }),
                postcssNestedAncestors,
                precss,
                lost,
                postcssCalc,
                postcssRgb,
                postcssFlexbugsFixes(),
                autoprefixer()
            ]
        }
    }
};

module.exports = {
    mode,
    target,
    entry: "./src/index.tsx",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "js/[name].js",
        chunkFilename: 'js/[name].chunk.js.js'
    },
    module: {
      rules: [
          {
              test: /\.(ico|jpe?g|png|webp|svg)$/,
              exclude: [/node_modules/],
              type: 'asset/resource',
              generator: {
                  filename: 'images/[hash][ext][query]'
              }
          },
          {
              test: /\.css$/i,
              use: [
                  {
                      loader: "@ecomfe/class-names-loader"
                  },
                  {
                      loader: MiniCssExtractPlugin.loader,
                      options: {
                          publicPath: ""
                      }
                  },
                  {
                      loader: "css-loader",
                      options: {
                          modules: {
                              ...cssLoaderModulesOptions
                          },
                          importLoaders: 1
                      }
                  },
                  postCssLoader]
          },

          {
              test: /\.(js|ts)x?$/,
              exclude: /node_modules/,
              use: {
                  loader: "babel-loader"
              }
          }
      ]
    },
    devtool: mode === "development" ? "eval-cheap-module-source-map" : false,

    optimization: {
        splitChunks: {
            chunks: 'all'
        },
        runtimeChunk: {
            name: 'manifest'
        },
        sideEffects: true,
        minimizer: mode === "development"
        ? [] :
            [new TerserPlugin({
                terserOptions: {
                    compress: {
                        ecma: 5,
                        warnings: false,
                        comparisons: false
                    },
                    mangle: {
                        safari10: true
                    },
                    format: {
                        ecma: 5,
                        comments: false,
                        ascii_only: true,
                        safari10: true,
                        webkit: true
                    }
                },
                parallel: true,
                extractComments: false
            }),
                new CssMinimizerPlugin({
                    minimizerOptions: {
                        preset: [
                            'default',
                            {
                                discardComments: { removeAll: true }
                            }
                        ]
                    }
                })]
    },

    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin(),
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        }),
        ...(
            isHotModuleReplacementEnabled
            ? [new ReactRefreshWebpackPlugin()]
                : []
        )],

    resolve: {
        extensions: [".js", ".jsx", ".tsx", ".ts"],
        alias: {
            "app": path.resolve(__dirname, "src", "app")
        }
    },

    devServer: {
        static: "./dist",
        hot: true
    }
}